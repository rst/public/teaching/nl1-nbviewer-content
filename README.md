[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.hrz.tu-chemnitz.de%2Frst%2Fpublic%2Fteaching%2Fnl1-nbviewer-content/binder_public)

# Jupyter-Notebooks zur Lehrveranstaltung Nichtlineare Regelungstechnik 1

Dieses Repositorium enthält Jupyter-Notebooks begleitend zur Lehrveranstaltung
Nichtlineare Regelungstechnik 1 am [Institut für Regelungs- und
Steuerungstheorie](https://tu-dresden.de/ing/elektrotechnik/rst), TU Dresden.

Nachfolgend Links zur Ansicht der verfügbaren Notebooks (über [https://nbviewer.jupyter.org](https://nbviewer.jupyter.org),
keine eigene Python-Installation nötig, Notebooks werden aber nur statisch angezeigt. Für lauffähige Versionen siehe obiger mybinder-Link oder selbst runterladen und nutzen).

## Vorlesungen

- [V1: Beispiel Grenzzyklen](https://nbviewer.org/urls/gitlab.hrz.tu-chemnitz.de/rst/public/teaching/nl1-nbviewer-content/-/raw/master/vorlesungen/01_intro/nl1-v1-01-grenzzykl-vanderPol.ipynb)

- [V1: Beispiel 1 Chaos](https://nbviewer.org/urls/gitlab.hrz.tu-chemnitz.de/rst/public/teaching/nl1-nbviewer-content/-/raw/master/vorlesungen/01_intro/nl1-v1-02-chaos.ipynb)

- [V1: Beispiel 2 Chaos](https://nbviewer.org/urls/gitlab.hrz.tu-chemnitz.de/rst/public/teaching/nl1-nbviewer-content/-/raw/master/vorlesungen/01_intro/nl1-v1-03-chaos-zusatz.ipynb)

- [V1: Beispiel Bifurkation](https://nbviewer.org/urls/gitlab.hrz.tu-chemnitz.de/rst/public/teaching/nl1-nbviewer-content/-/raw/master/vorlesungen/01_intro/nl1-v1-04-bifurkation.ipynb)

- [V3: Methode der harmonischen Balance (Einführungsbeispiel)](https://nbviewer.org/urls/gitlab.hrz.tu-chemnitz.de/rst/public/teaching/nl1-nbviewer-content/-/raw/master/vorlesungen/03_harmbal/nl1-v3-harmbal-introexample.ipynb)

- [V6: Gleitregime-Regelung (Sliding-Mode-Control) - Grundaufgabe](https://nbviewer.org/urls/gitlab.hrz.tu-chemnitz.de/rst/public/teaching/nl1-nbviewer-content/-/raw/master/vorlesungen/06_slidingmode/nl1-v6-01-slidingmode-beispiel.ipynb)

- [V6: Gleitregime-Regelung (Sliding-Mode-Control) - robuste Auslegung](https://nbviewer.org/urls/gitlab.hrz.tu-chemnitz.de/rst/public/teaching/nl1-nbviewer-content/-/raw/master/vorlesungen/06_slidingmode/nl1-v6-02-slidingmode-beispiel.ipynb)

- [V6: Gleitregime-Regelung (Sliding-Mode-Control) - Trajektorienfolgeregelung](https://nbviewer.org/urls/gitlab.hrz.tu-chemnitz.de/rst/public/teaching/nl1-nbviewer-content/-/raw/master/vorlesungen/06_slidingmode/nl1-v6-03-slidingmode-beispiel.ipynb)

## Übungen

- [U2: Analyse nichtlinearer Systeme in der Nähe ihrer Ruhelagen](https://nbviewer.org/urls/gitlab.hrz.tu-chemnitz.de/rst/public/teaching/nl1-nbviewer-content/-/raw/master/uebungen/zustandsebene/nl1-u2-zustandsebene.ipynb)

- [U4: Anwendung des Satzes von Chetaev](https://nbviewer.org/urls/gitlab.hrz.tu-chemnitz.de/rst/public/teaching/nl1-nbviewer-content/-/raw/master/uebungen/ljapunov/nl1-u4-chetaev-illustration.ipynb)

- [U4: Anwendung des Satzes von LaSalle](https://nbviewer.org/urls/gitlab.hrz.tu-chemnitz.de/rst/public/teaching/nl1-nbviewer-content/-/raw/master/uebungen/ljapunov/nl1-u4-lasalle-h1-h2.ipynb)

- [U5: Robuster Reglerentwurf nach dem Backstepping-Verfahren](https://nbviewer.jupyter.org/urls/gitlab.hrz.tu-chemnitz.de/rst/public/teaching/nl1-nbviewer-content/-/raw/master/uebungen/backstepping/nl1-u5-backstepping-robust.ipynb)

- [U6: Entwurf eines robusten Sliding-Mode-Reglers](https://nbviewer.jupyter.org/urls/gitlab.hrz.tu-chemnitz.de/rst/public/teaching/nl1-nbviewer-content/-/raw/master/uebungen/sliding-mode-control/nl1-u6-sliding-mode-robust.ipynb)

- [U7: Exakte Eingangs-Ausgangs-Linearisierung (akademisches Beispiel)](https://nbviewer.jupyter.org/urls/gitlab.hrz.tu-chemnitz.de/rst/public/teaching/nl1-nbviewer-content/-/raw/master/uebungen/io-lin/nl1-u7-iolin-asinus-system.ipynb)

- [U7: Exakte Eingangs-Ausgangs-Linearisierung (CSTR)](https://nbviewer.jupyter.org/urls/gitlab.hrz.tu-chemnitz.de/rst/public/teaching/nl1-nbviewer-content/-/raw/master/uebungen/io-lin/nl1-u7-iolin-cstr.ipynb)

- [U7: Exakte Eingangs-Ausgangs-Linearisierung (Vergleich)](https://nbviewer.jupyter.org/urls/gitlab.hrz.tu-chemnitz.de/rst/public/teaching/nl1-nbviewer-content/-/raw/master/uebungen/io-lin/nl1-u7-iolin-vergleich.ipynb)
